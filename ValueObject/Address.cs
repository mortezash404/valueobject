﻿namespace ValueObject
{
    public class Address
    {
        public string State { get; set; }
        public string City { get; set; }
    }
}