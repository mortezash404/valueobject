﻿using Microsoft.EntityFrameworkCore;

namespace ValueObject
{
    public class AppDbContext : DbContext
    {
        public DbSet<Person> Persons { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=.;Database=ValueDb;Trusted_Connection=True;");

            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Person>().ToTable("Person");

            modelBuilder.Entity<Person>().Ignore(i => i.Address);

            base.OnModelCreating(modelBuilder);
        }
    }
}
