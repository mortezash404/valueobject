﻿using System;
using System.Text.Json;

namespace ValueObject
{
    class Program
    {
        static void Main(string[] args)
        {
            var context = new AppDbContext();

            context.Database.EnsureCreated();

            // Insert

            context.Persons.Add(new Person
            {
                Name = "Reza",
                AddressBody = JsonSerializer.Serialize(new Address
                {
                    State = "Fars",
                    City = "Shiraz"
                })
            });

            context.SaveChanges();


            // Select

            var person = context.Persons.Find(3);

            person.Address = JsonSerializer.Deserialize<Address>(person.AddressBody);

            Console.WriteLine($"Name is : {person.Name}, State is : {person.Address.State}, City is : {person.Address.City}");
        }
    }
}
