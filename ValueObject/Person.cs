﻿namespace ValueObject
{
    public class Person
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public Address Address { get; set; }

        public string AddressBody { get; set; }
    }
}
